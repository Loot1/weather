import * as React from 'react'
import { Platform, Text, ImageBackground, SafeAreaView, View, StatusBar } from "react-native"
import { SearchBar } from "react-native-elements"
//import Geolocation from '@react-native-community/geolocation'

const fetch = require("node-fetch")

import APIkey from "./Settings.js"

export default function App() {
    async function getWeather(city = "Amiens") {
        var loc = await getCoordinates(city)
        if(loc.length === 0) {
            city = "Amiens"
            loc = await getCoordinates(city)
        }
        const response = await fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${loc[0].lat}&lon=${loc[0].lon}&lang=fr&units=metric&exclude=minutely,hourly,alerts&appid=${APIkey}`)
        var data = await response.json()
        data.city = city
        return data
    }
    async function getCoordinates(city) {
        const response = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${city}&appid=${APIkey}`)
        if (response.ok) return await response.json()
    }
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1)
    }
    //const [geo, setGeo] = React.useState(null)
    const [search, setSearch] = React.useState("")
    const [weather, setWeather] = React.useState({city:"Chargement...",temp:0, min:0, max:0, desc:"Chargement...", icon:"01d", wind:0, humidity:0, feel:0, uv:0})
    const list = [{color:"#000000",icon:require("./img/01d.gif"),name:"01d"},{color:"#eeeeee",icon:require("./img/01n.gif"),name:"01n"},{color:"#000000",icon:require("./img/d.gif"),name:"02d"},{color:"#eeeeee",icon:require("./img/n.gif"),name:"02n"},{color:"#000000",icon:require("./img/d.gif"),name:"03d"},{color:"#eeeeee",icon:require("./img/n.gif"),name:"03n"},{color:"#000000",icon:require("./img/d.gif"),name:"04d"},{color:"#eeeeee",icon:require("./img/n.gif"),name:"04n"},{color:"#000000",icon:require("./img/09.gif"),name:"09d"},{color:"#000000",icon:require("./img/09.gif"),name:"09n"},{color:"#eeeeee",icon:require("./img/10.gif"),name:"10d"},{color:"#eeeeee",icon:require("./img/10.gif"),name:"10n"},{color:"#000000",icon:require("./img/11.gif"),name:"11d"},{color:"#000000",icon:require("./img/11.gif"),name:"11n"},{color:"#000000",icon:require("./img/13.gif"),name:"13d"},{color:"#000000",icon:require("./img/13.gif"),name:"13n"},{color:"#000000",icon:require("./img/50.gif"),name:"50d"},{color:"#000000",icon:require("./img/50.gif"),name:"50n"}]
    React.useEffect(() => {
        //Geolocation.getCurrentPosition(info => setGeo(info))
        getWeather().then(data => setWeather({
            city:data.city,
            temp:data.current.temp,
            min:data["daily"][0].temp.min,
            max:data["daily"][0].temp.max,
            desc:data.current.weather[0].description,
            icon:data.current.weather[0].icon,
            wind:data.current.wind_speed,
            humidity:data.current.humidity,
            feel:data.current.feels_like,
            uv:data.current.uvi
        }))
    }, [])
    return (
        <SafeAreaView style={{flex: 1}}>
            <StatusBar backgroundColor="transparent" translucent={true}/>
            <ImageBackground source={list.find(t => t.name === weather.icon).icon} style={{flex: 1, resizeMode:"cover"}}>
                <SearchBar
                    platform={Platform.OS === "ios" ? "ios" : "android"}
                    onChangeText={newVal => setSearch(newVal)}
                    placeholder="Ville"
                    placeholderTextColor={list.find(t => t.name === weather.icon).color}
                    value={search}
                    clearIcon={{color:list.find(t => t.name === weather.icon).color}}
                    cancelIcon={{color:list.find(t => t.name === weather.icon).color}}
                    searchIcon={{color:list.find(t => t.name === weather.icon).color, onPress:() => {
                        if(weather.city.toLowerCase() != search.toLowerCase() && search.length > 0) {
                            getWeather(search).then(data => setWeather({
                                city:data.city,
                                temp:data.current.temp,
                                min:data["daily"][0].temp.min,
                                max:data["daily"][0].temp.max,
                                desc:data.current.weather[0].description,
                                icon:data.current.weather[0].icon,
                                wind:data.current.wind_speed,
                                humidity:data.current.humidity,
                                feel:data.current.feels_like,
                                uv:data.current.uvi
                            }))
                        }
                    }}}
                    inputStyle={{color:list.find(t => t.name === weather.icon).color}}
                    inputContainerStyle={{flexDirection:"row-reverse"}}
                    containerStyle={{backgroundColor:"transparent", marginTop:80, borderColor:list.find(t => t.name === weather.icon).color, borderWidth:1, borderRadius:30, marginHorizontal:40, padding:20}}
                />
                <View style={{alignItems:"center", justifyContent:"center", marginTop: 90}}>
                    <Text style={{fontSize:34, color:list.find(t => t.name === weather.icon).color}}>
                        {weather.city}
                    </Text>
                    <Text style={{fontSize:14, color:list.find(t => t.name === weather.icon).color}}>
                        {capitalizeFirstLetter(weather.desc)}
                    </Text>
                    <Text style={{fontSize:70, color:list.find(t => t.name === weather.icon).color}}>
                        {Math.round(weather.temp)}°
                    </Text>
                    <Text style={{fontSize:16, color:list.find(t => t.name === weather.icon).color}}>
                        Max. {Math.round(weather.max)}° Min. {Math.round(weather.min)}°
                    </Text>
                    <View style={{marginTop:80}}>
                        <Text style={{fontSize:24, color:list.find(t => t.name === weather.icon).color}}>
                            🌬️ {Math.round(weather.wind)} km/h
                            💧 {weather.humidity} %
                        </Text>
                        <Text style={{fontSize:24, color:list.find(t => t.name === weather.icon).color, marginTop: 50}}>
                            Ressenti {Math.round(weather.feel)}°
                            ☀️ {weather.uv}
                        </Text>
                    </View>
                </View>
            </ImageBackground>
        </SafeAreaView>
    )
}